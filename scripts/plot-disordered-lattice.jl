using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using Formatting
using ArgParse
using JSON
using CodecXz
using MsgPack
using PyCall
const npl = pyimport("numpy.linalg")
using PyPlot
const mpl = pyimport("matplotlib")

using LatticeTools
using QuantumHamiltonian

using DisorderedKagome


"""
    plot_lattice(directory, output_filename)

Draw the Kagome lattice (with obc) defined by `[directory]/lattice.json`,
and save it to output_filename. Uses PyPlot, so the file format depends
on the extension.
"""
function plot_lattice(
    directory::AbstractString,
    output_filename::AbstractString,
)
    lattice_data = JSON.parsefile(joinpath(directory, "lattice.json"))
    sites = [(sitetype, [R...]) for (sitetype, R) in lattice_data["sites"]]
    kagome = make_kagome_obc(sites)
    n_sites = numsite(kagome.unitcell)
    (new_kagome_supercell, nn_bonds, nn_triangles) = kagome

    sitecoords = hcat([fract2carte(new_kagome_supercell, sitefc) for (sitename, sitefc) in new_kagome_supercell.sites]...)

    fig = PyPlot.figure(figsize=(5.5, 4))
    ax = fig.gca()

    # draw triangles
    patches = []
    for (i, j, k) in nn_triangles
        Ri = fract2carte(new_kagome_supercell, getsitecoord(new_kagome_supercell, i))
        Rj = fract2carte(new_kagome_supercell, getsitecoord(new_kagome_supercell, j))
        Rk = fract2carte(new_kagome_supercell, getsitecoord(new_kagome_supercell, k))
        xy = transpose(hcat(Ri, Rj, Rk))
        patch = mpl.patches.Polygon(xy, closed=true)
        push!(patches, patch)
    end
    p = mpl.collections.PatchCollection(patches, color="xkcd:royal blue", alpha=0.5)
    ax.add_collection(p)

    ax.scatter(sitecoords[1, :], sitecoords[2, :], color="black")
    ax.set_xlim(minimum(sitecoords[1, :]) - 0.1, maximum(sitecoords[1, :]) + 0.1)
    ax.set_ylim(minimum(sitecoords[2, :]) - 0.1, maximum(sitecoords[2, :]) + 0.1)
    ax.set_aspect(1.0)

    output_directory = dirname(output_filename)
    if !isdir(output_directory)
        mkpath(output_directory)
    end
    fig.savefig(output_filename, dpi=300, bbox_inches="tight")
end

function parse_commandline()
    s = ArgParseSettings(
        description="plot obc lattice"
    )
    @add_arg_table! s begin
        "directory"
            arg_type = String
            required = true
        "--out", "-o"
            arg_type = String
            required = true
    end
    return parse_args(s)
end

function main()
    parsed_args = parse_commandline()
    plot_lattice(parsed_args["directory"], parsed_args["out"])
end

main()
