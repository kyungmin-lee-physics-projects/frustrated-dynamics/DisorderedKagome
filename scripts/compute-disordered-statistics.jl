using LinearAlgebra
using Statistics
using Glob
using CodecXz
using MsgPack
using Formatting
using ProgressMeter
using ArgParse
using DataFrames
using CSV


"""
    get_spacings(values, [digits=8])

Compute spacings between consecutive values, after rounding by digits and then sorting.
"""
function get_spacings(values::AbstractVector{<:Real}, digits::Integer=8)
    values2 = round.(sort(values); digits=digits)
    spacings = values2[2:end] - values2[1:end-1]
    return spacings
end


"""
    get_ratios(values, [digits=8])

Compute the ratios between spacings. Uses [`get_spacing`](@ref).
If the spacing is zero, use the next non-zero value.
"""
function get_ratios(values::AbstractVector{T}, digits::Integer=8) where {T<:AbstractFloat}
    spacings = get_spacings(values, digits)
    n = length(spacings)
    n < 2 && return T[]

    m = n
    while m > 1 && spacings[m] == 0
        m -= 1
    end

    m == 1 && return T[]

    s_prev = spacings[m] # nonzero
    ratios = T[]
    while m > 1
        m -= 1
        s = spacings[m]
        r = min(s, s_prev) / max(s, s_prev)
        if s != 0
            s_prev = s
        end
        push!(ratios, r)
        #print('mm=', m, 'len =', len(ratios))
    end
    return ratios
end


"""
    Form of GOE ratio function.
"""
function pdf_general(beta::Real)
    function pdf(x::Real)
        return (27.0/4) * (x+x^2)^(beta) / (1+x+x^2)^(1+3*beta/2)
    end
    # function pdf(x::AbstractArray{<:Real})
    #     return (27.0/4) .* (x + x.^2).^(beta) ./ (1 + x + x .^ 2)^(1 + 3 .* beta./2)
    # end
    return pdf
end

pdf_goe = pdf_general(1)
pdf_poisson(x::Real) = 2 / (1+x)^2
#pdf_poisson(x::AbstractArray{<:Real}) = 2 ./ (1+x).^2



"""
    read_directory(lattice_type, shape_str)

Read all *.msgpack.xz in directory `[datadir]/[lattice_type]/[shape_str]`.

# Return
* dictionary
  `(Jx, Jy, Jz, J1, J2, Sz) -> [[eigenvalues for one seed], [eigenvalues for another seed], ...]`.
  `J1` is `[J1min, J1max]`, and similarly for `J2`.
"""
function read_directory(lattice_type::AbstractString, shape_str::AbstractString)
    filepaths = glob("*.msgpack.xz", joinpath(@__DIR__, "..", "data", lattice_type, shape_str));

    database = Dict()

    println("Reading $(length(filepaths)) files")
    @showprogress for filepath in filepaths
        open(filepath, "r") do io
            ioc = XzDecompressorStream(io)
            data = MsgPack.unpack(ioc)
            close(ioc)
            parameter = data["parameter"]
            sector = data["sector"]
            J1 = Float64[parameter["J1"]...]
            J2 = Float64[parameter["J2"]...]
            Jx = Float64(parameter["Jx"])
            Jy = Float64(parameter["Jy"])
            Jz = Float64(parameter["Jz"])
            #shape = Int[parameter["shape"]...]
            seed = Int(parameter["seed"])

            Sz = Float64(sector["Sz"])
            dimension = Int(sector["dimension"])
            matrix_type = String(sector["matrix_type"])

            p = (Jx, Jy, Jz, J1, J2, Sz)
            if !haskey(database, p)
                database[p] = Vector{Float64}[]  # just eigenvalues
            end
            push!(database[p], data["eigenvalue"])
        end
    end
    return database
end


"""
    plot_window(lattice_type::AbstractString, shape_str::AbstractString, database::AbstractDict)

Plot windowed level statistics in `database`, and save it to
`[datadir]/[lattice_type]/[shape_str]`.
"""
function compute_window(lattice_type::AbstractString, shape_str::AbstractString, database::AbstractDict; window::Real=0.05)
    xs = 0:0.01:1
    ys_goe = pdf_goe.(xs)
    ys_poi = pdf_poisson.(xs)

    portions = 0:window:1
    portion_lowers = portions[1:end-1]
    portion_uppers = portions[2:end]

    # portion_lowers = [
    #     0.00, 0.05, 0.10, 0.15,
    #     0.20, 0.25, 0.30, 0.35,
    #     0.40, 0.45, 0.50, 0.55,
    #     0.60, 0.65, 0.70, 0.75,
    #     0.80, 0.85, 0.90, 0.95,
    # ]
    # portion_uppers = [
    #     0.05, 0.10, 0.15, 0.20,
    #     0.25, 0.30, 0.35, 0.40,
    #     0.45, 0.50, 0.55, 0.60,
    #     0.65, 0.70, 0.75, 0.80,
    #     0.85, 0.90, 0.95, 1.00,
    # ]

    for ((Jx, Jy, Jz, (J1min, J1max), (J2min, J2max), Sz), all_eigenvalues) in database
        out_filename = let
            prefix = "level-statistics-ratio-portion"
            parameter = format(
                "J1=({:.3f},{:.3f})_J2=({:.3f},{:.3f})_Jx={:.3f}_Jy={:.3f}_Jz={:.3f}_Sz={:.1f}_shape={:s}_window={:.3f}",
                J1min, J1max, J2min, J2max, Jx, Jy, Jz, Sz, shape_str, window
            )
            "$(prefix)_$(parameter).csv"
        end
        out_filepath = joinpath(@__DIR__, "..", "data", lattice_type, shape_str, out_filename)
        out_filedir = dirname(out_filepath)

        #!isdir(out_filedir) && mkpath(out_filedir)

        ratios = [Vector{Float64}[] for p in portion_uppers]

        dimension = length(first(all_eigenvalues))
        count = length(all_eigenvalues)
        for eigenvalues in all_eigenvalues
            eigenvalue_min = minimum(eigenvalues)
            eigenvalue_max = maximum(eigenvalues)
            idx_select = [
                let lo = ((eigenvalue_max - eigenvalue_min) * portion_lowers[ir]) + eigenvalue_min,
                    hi = ((eigenvalue_max - eigenvalue_min) * portion_uppers[ir]) + eigenvalue_min
                    [lo <= ev <= hi for ev in eigenvalues]
                end
                    for ir in eachindex(portion_uppers)
            ]
            for (i, idx) in enumerate(idx_select)
                push!(ratios[i], collect(get_ratios(Float64[eigenvalues[idx]...])))
            end
        end

        output_data = []
        for (i, (plo, phi)) in enumerate(zip(portion_lowers, portion_uppers))
            vals = vcat(ratios[i]...)
            if isempty(vals)
                push!(output_data, (low=plo, high=phi, count=0, mean=NaN, mean_err=NaN))
            else
                push!(output_data, (low=plo, high=phi, count=length(vals), mean=mean(vals), mean_err=std(vals) / sqrt(length(vals)) ))
            end
        end
        df = DataFrame(output_data)
        CSV.write(out_filepath, df)
    end
end


function parse_commandline()
    s = ArgParseSettings(
        prog="",
        description="Plot the statistics of a disordered Kagome lattice",
    )
    @add_arg_table! s begin
        "lattice_type"
            arg_type = String
            required = true
        "shape"
            arg_type = String
            required = true
            nargs = '+'
        "--window"
            arg_type = Float64
            default = 0.05
    end
    return parse_args(s)
end


function main()
    args = parse_commandline()
    for s in args["shape"]
        database = read_directory(args["lattice_type"], s)
        compute_window(args["lattice_type"], s, database; window=args["window"])
    end
end

main()
