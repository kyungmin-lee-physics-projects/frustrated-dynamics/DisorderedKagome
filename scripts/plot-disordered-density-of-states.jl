using LinearAlgebra
using Statistics
using Plots
import PyPlot
using Glob
using CodecXz
using MsgPack
using Formatting
using ProgressMeter
using ArgParse
PyPlot.matplotlib.use("Agg")

"""
    read_directory(lattice_type, shape_str)

Read all *.msgpack.xz in directory `[datadir]/[lattice_type]/[shape_str]`.

# Return
* dictionary
  `(Jx, Jy, Jz, J1, J2, Sz) -> [[eigenvalues for one seed], [eigenvalues for another seed], ...]`.
  `J1` is `[J1min, J1max]`, and similarly for `J2`.
"""
function read_directory(lattice_type::AbstractString, shape_str::AbstractString)
    filepaths = glob("*.msgpack.xz", joinpath(@__DIR__, "..", "data", lattice_type, shape_str));

    database = Dict()

    println("Reading $(length(filepaths)) files")
    @showprogress for filepath in filepaths
        open(filepath, "r") do io
            ioc = XzDecompressorStream(io)
            data = MsgPack.unpack(ioc)
            close(ioc)
            parameter = data["parameter"]
            sector = data["sector"]
            J1 = Float64[parameter["J1"]...]
            J2 = Float64[parameter["J2"]...]
            Jx = Float64(parameter["Jx"])
            Jy = Float64(parameter["Jy"])
            Jz = Float64(parameter["Jz"])
            #shape = Int[parameter["shape"]...]
            seed = Int(parameter["seed"])

            Sz = Float64(sector["Sz"])
            dimension = Int(sector["dimension"])
            matrix_type = String(sector["matrix_type"])

            p = (Jx, Jy, Jz, J1, J2, Sz)
            if !haskey(database, p)
                database[p] = Vector{Float64}[]  # just eigenvalues
            end
            push!(database[p], data["eigenvalue"])
        end
    end
    return database
end



"""
    plot_density_of_states(lattice_type, shape_str, database)

Plot density of states.
`[datadir]/[lattice_type]/[shape_str]`.
"""
function plot_density_of_states(
    lattice_type::AbstractString,
    shape_str::AbstractString,
    database::AbstractDict;
    extensions::AbstractVector{<:AbstractString}=["pdf"]
)
    fig = PyPlot.figure(figsize=(4, 3.5))

    println("Plotting $(length(database)) items")
    @showprogress for ((Jx, Jy, Jz, (J1min, J1max), (J2min, J2max), Sz), all_eigenvalues) in database
        dimension = length(first(all_eigenvalues))
        count = length(all_eigenvalues)

        #bins = min(50, floor(Int, length(vals)/16)+1)
        eigenvalues = vcat(all_eigenvalues...)
        if isempty(eigenvalues)
            continue
        end
        @show (Jx, Jy, Jz, (J1min, J1max), (J2min, J2max), Sz)

        fig.clf()
        ax = fig.gca()
        ax.hist(eigenvalues, bins=100, density=true)
        ax.set_xlabel(raw"$E$", fontsize=12)
        ax.set_ylabel(raw"$\rho(E)$", fontsize=12)

        fig.suptitle("$(lattice_type) $shape_str. \$ J_{z} = $(Jz), \\quad J_{1} \\in [$(J1min), $(J1max)), \\quad J_{2} \\in [$(J2min), $(J2max)) \$, dim = $(dimension), count = $(count)", fontsize=6)

        for ext in extensions
            out_filename = let
                prefix = "density-of-states"
                parameter = format(
                    "J1=({:.3f},{:.3f})_J2=({:.3f},{:.3f})_Jx={:.3f}_Jy={:.3f}_Jz={:.3f}_Sz={:.1f}_shape={:s}",
                    J1min, J1max, J2min, J2max, Jx, Jy, Jz, Sz, shape_str
                )
                "$(prefix)_$(parameter).$(ext)"
            end

            out_filepath = joinpath(@__DIR__, "..", "plots", lattice_type, shape_str, out_filename)
            out_filedir = dirname(out_filepath)

            !isdir(out_filedir) && mkpath(out_filedir)
            fig.savefig(out_filepath, dpi=300, bbox_inches="tight")
        end
    end
end


function parse_commandline()
    s = ArgParseSettings(
        prog="",
        description="Plot the statistics of a disordered Kagome lattice",
    )
    @add_arg_table! s begin
        "lattice_type"
            arg_type = String
            required = true
        "shape"
            arg_type = String
            required = true
            nargs = '+'
        "--ext"
            arg_type = String
            default = ["pdf"]
            nargs = '+'
    end
    return parse_args(s)
end


function main()
    args = parse_commandline()
    for s in args["shape"]
        database = read_directory(args["lattice_type"], s)
        plot_density_of_states(args["lattice_type"], s, database; extensions=args["ext"])
    end
end

main()
