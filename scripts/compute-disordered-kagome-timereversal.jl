using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

#include(srcdir("Preamble.jl"))
#include(srcdir("Kagome.jl"))
using DisorderedKagome

using Random
using LinearAlgebra
using DataStructures

using Logging
using Printf
using Formatting
using ArgParse

using CodecXz
using MsgPack
using JSON
using BitIntegers

using LatticeTools
using QuantumHamiltonian
using Arpack

using PyCall
npl = pyimport("numpy.linalg")

struct FakeIrrepComponent <: AbstractSymmetryIrrepComponent end

function reduce_spinflip_symmetry(
    hilbert_space_representation::HilbertSpaceRepresentation{HS, BR, DT},
    spin_flip_sign::Integer,
) where {HS, BR, DT}
    ComplexType = ComplexF64
    n_sites = length(basespace(hilbert_space_representation).sites)
    mask = make_bitmask(n_sites, BR)
    @assert count_ones(mask) == n_sites
    spinflipoperation(bvec::BR) = mask & (~bvec)

    hsr = hilbert_space_representation
    HSR = typeof(hsr)
    n_basis = dimension(hsr)

    basis_mapping_representative = Vector{Int}(undef, n_basis)
    fill!(basis_mapping_representative, -1)
    basis_mapping_amplitude = zeros(Float64, n_basis)

    group_size = 2
    symops_and_amplitudes = [(identity, 1), (spinflipoperation, spin_flip_sign)]
    is_identity = [true, spin_flip_sign == 1]

    reduced_basis_list = BR[]
    sizehint!(reduced_basis_list, n_basis ÷ 2 + n_basis ÷ 4)

    visited = falses(n_basis)

    basis_states = Vector{BR}(undef, group_size)
    basis_amplitudes = Dict{BR, ComplexType}()
    sizehint!(basis_amplitudes, 2)

    for ivec_p in 1:n_basis
        visited[ivec_p] && continue
        bvec = hsr.basis_list[ivec_p]

        compatible = true
        for i in 2:2
            (symop, _) = symops_and_amplitudes[i]
            bvec_prime = symop(bvec)
            if bvec_prime < bvec
                compatible = false
                break
            elseif bvec_prime == bvec && !is_identity[i]
                compatible = false
                break
            end
            basis_states[i] = bvec_prime
        end
        (!compatible) && continue
        basis_states[1] = bvec

        push!(reduced_basis_list, bvec)

        empty!(basis_amplitudes)
        for i in 1:group_size
            (_, ampl) = symops_and_amplitudes[i]
            bvec_prime = basis_states[i]
            basis_amplitudes[bvec_prime] = ampl
        end
        inv_norm = inv(sqrt(float(length(basis_amplitudes))))
        for (bvec_prime, amplitude) in basis_amplitudes
            ivec_p_prime = hsr.basis_lookup[bvec_prime]
            visited[ivec_p_prime] = true
            basis_mapping_representative[ivec_p_prime] = ivec_p
            basis_mapping_amplitude[ivec_p_prime] = amplitude * inv_norm
        end
    end

    basis_mapping_index = Vector{Int}(undef, n_basis)
    fill!(basis_mapping_index, -1)

    for (ivec_r, bvec) in enumerate(reduced_basis_list)
        ivec_p = hsr.basis_lookup[bvec]
        basis_mapping_index[ivec_p] = ivec_r
    end

    for (ivec_p_prime, ivec_p) in enumerate(basis_mapping_representative)
        (ivec_p <= 0) && continue
        (ivec_p_prime == ivec_p) && continue
        ivec_r = basis_mapping_index[ivec_p]
        basis_mapping_index[ivec_p_prime] = ivec_r
    end

    RHSR = ReducedHilbertSpaceRepresentation{
        HSR,
        FakeIrrepComponent,
        BR,
        ComplexType
    }

    rhsr = RHSR(
        hsr, FakeIrrepComponent(),
        reduced_basis_list, basis_mapping_index, basis_mapping_amplitude,
    )
    return rhsr
end


function compute_disordered_kagome_trs(
    Jx::Real, Jy::Real, Jz ::Real,
    J1min::Real, J1max::Real,
    J2min::Real, J2max::Real,
    shape::AbstractMatrix{<:Integer},
    max_dense::Integer, nev::Integer,
    random_seed::Integer,
    measure::Bool,
    force::Bool=true
)

    @mylogmsg "lattice shape: $shape"
    @mylogmsg "Jx: $Jx"
    @mylogmsg "Jy: $Jy"
    @mylogmsg "Jz: $Jz"
    @mylogmsg "J1: $J1min -- $J1max"
    @mylogmsg "J2: $J2min -- $J2max"

    n11 = shape[1,1]
    n12 = shape[1,2]
    n21 = shape[2,1]
    n22 = shape[2,2]

    kagome = make_kagome_lattice(shape)

    n_sites = numsite(kagome.lattice.supercell)
    @mylogmsg "Number of sites: $n_sites"

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)

    @mylogmsg "Generating spin operators"
    sx = 0.5 * sum(pauli(i, :x) for i in 1:n_sites)
    sy = 0.5 * sum(pauli(i, :y) for i in 1:n_sites)
    sz = 0.5 * sum(pauli(i, :z) for i in 1:n_sites)
    spin_squared = simplify(sx*sx + sy*sy + sz*sz)

    @mylogmsg "Generating Hamiltonian"
    rng = MersenneTwister(random_seed)

    jx, jy, jz = NullOperator(), NullOperator(), NullOperator()
    for (tri, sgn) in kagome.nearest_neighbor_triangles
        ampl = J1min + (J1max - J1min) * rand(rng, Float64)
        for ((i, j), R) in tri
            jx += pauli(i, :x) * pauli(j, :x) * ampl
            jy += pauli(i, :y) * pauli(j, :y) * ampl
            jz += pauli(i, :z) * pauli(j, :z) * ampl
        end
    end
    j1 = simplify(Jx*jx + Jy*jy + Jz*jz) * 0.25

    jx2, jy2, jz2 = NullOperator(), NullOperator(), NullOperator()
    for (tri, sgn) in kagome.next_nearest_neighbor_triangles
        ampl = J2min + (J2max - J2min) * rand(rng, Float64)
        for ((i, j), R) in tri
            jx2 += pauli(i, :x) * pauli(j, :x) * ampl
            jy2 += pauli(i, :y) * pauli(j, :y) * ampl
            jz2 += pauli(i, :z) * pauli(j, :z) * ampl
        end
    end
    j2 = simplify(Jx*jx2 + Jy*jy2 + Jz*jz2) * 0.25

    hamiltonian = simplify(j1 + j2)
    @mylogmsg "Quantum numbers: $(quantum_number_sectors(hs))"

    @mylogmsg "Creating Hilbert space sector"
    hss = HilbertSpaceSector(hs, 0)
    hssr = represent_dict(hss)
    @mylogmsg "Complete dimension (no Z2) = $(dimension(hssr))"

    for spinflip in [1, -1]
        Sz = 0.0
        @mylogmsg "spinflip: $spinflip"
        parameter_filename = Dict{Symbol, Any}(
            :shape=>"($n11,$n21)x($n12,$n22)",
            :Jx=>@sprintf("%.3f", Jx),
            :Jy=>@sprintf("%.3f", Jy),
            :Jz=>@sprintf("%.3f", Jz),
            :J1=>@sprintf("(%.3f,%.3f)", J1min, J1max),
            :J2=>@sprintf("(%.3f,%.3f)", J2min, J2max),
            :Sz=>@sprintf("%.1f", Sz),
            :spinflip=>spinflip,
            :seed=>random_seed,
        )
        if measure
            output_filename = savename("spectrum-disordered-dense-measure", parameter_filename, "msgpack.xz")
        else
            output_filename = savename("spectrum-disordered-dense", parameter_filename, "msgpack.xz")
        end
        output_filepath = datadir("kagome", "($n11,$n21)x($n12,$n22)", output_filename)
        if ispath(output_filepath)
            @mylogmsg "File $output_filepath exists."
            if force
                @mylogmsg "Overwriting."
            else
                @mylogmsg "Skipping."
                continue
            end
        end

        rhssr = reduce_spinflip_symmetry(hssr, spinflip)

        hilbert_space_dimension = dimension(rhssr)
        @mylogmsg "Hilbert space dimension: $hilbert_space_dimension"

        hilbert_space_dimension == 0 && continue
        @mylogmsg "Will save results to $output_filepath"

        if hilbert_space_dimension > max_dense
            matrix_type = "sparse"
        else
            matrix_type = "dense"
        end
        @mylogmsg "matrix type: $matrix_type"

        spin_squared_rep = represent(rhssr, spin_squared)
        hamiltonian_rep = represent(rhssr, hamiltonian)

        if matrix_type == "sparse"
            @mylogmsg "Diagonalizing sparse Hamiltonian"
            eigenvalues, eigenvectors = eigs(hamiltonian_rep; nev=nev, which=:SR)
            eigenvalues = real.(eigenvalues)
        else  # if matrix_type == "dense"
            @mylogmsg "Creating dense Hamiltonian matrix"
            hamiltonian_dense = Matrix(hamiltonian_rep)
            @mylogmsg "Diagonalizing dense Hamiltonian matrix"
            if hilbert_space_dimension < 40000
                eigenvalues, eigenvectors = npl.eigh(hamiltonian_dense)
            else
                eigenvalues, eigenvectors = eigen(Hermitian(hamiltonian_dense))
            end
        end

        spin_two_list = Float64[]
        spin_four_list = Float64[]
        if measure
            spin_two_list = zeros(Float64, length(eigenvalues))
            spin_four_list = zeros(Float64, length(eigenvalues))

            Threads.@threads for i_eigen in eachindex(eigenvalues)
                phi = eigenvectors[:, i_eigen]
                s2phi = spin_squared_rep * phi
                s4phi = spin_squared_rep * s2phi
                s2 = real(dot(phi, s2phi))
                s4 = real(dot(phi, s4phi))
                spin_two_list[i_eigen] = s2
                spin_four_list[i_eigen] = s4
            end # for i_eigen
        end

        @mylogmsg "Saving to $output_filepath"

        mkpath(dirname(output_filepath); mode=0o755)

        open(output_filepath, "w") do io
            ioc = XzCompressorStream(io)
            save_data = OrderedDict(
                "parameter" => OrderedDict(
                    "J1" => [J1min, J1max],
                    "J2" => [J2min, J2max],
                    "Jx" => Jx,
                    "Jy" => Jy,
                    "Jz" => Jz,
                    "shape" => [shape[1,1], shape[2,1], shape[1,2], shape[2,2]],
                    "seed" => random_seed,
                ),
                "sector" => OrderedDict(
                    "Sz"=>Sz,
                    "spinflip" => spinflip,
                    "dimension"=> hilbert_space_dimension,
                    "matrix_type" => matrix_type,
                ),
                "eigenvalue" => eigenvalues,
            )
            if measure
                save_data["spin_two"] = spin_two_list
                save_data["spin_four"] = spin_four_list
            end

            MsgPack.pack(ioc, save_data)
            close(ioc)
        end
    end
end





function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "--shape"
        arg_type = Int
        nargs = 4
        required = true
        help = "shape of the cluster in units of lattice vectors (a,b) x (c,d)"
        "--Jx"
        arg_type = Float64
        nargs = '*'
        default = [1.0]
        help = "values of Jx"
        "--Jy"
        arg_type = Float64
        nargs = '*'
        default = [1.0]
        help = "values of Jy"
        "--Jz"
        arg_type = Float64
        nargs = '+'
        help = "values of Jz"
        "--J1"
        arg_type = Float64
        nargs = 2
        required = true
        help = "values of J1"
        "--J2"
        arg_type = Float64
        nargs = 2
        required = true
        help = "values of J2"
        "--max-dense"
        arg_type = Int64
        default = 20000
        help = "maximum hilbert space dimension to solve with dense matrix"
        "--nev"
        arg_type = Int
        default = 300
        help = "number of eigenvalues to compute when using sparse"
        "--seed"
        arg_type = Int
        default = 1
        help = "random seed"
        "--debug", "-d"
        help = "debug"
        action = :store_true
        "--measure", "-m"
        help = "measure"
        action = :store_true
        "--force", "-f"
        help = "force run (overwrite)"
        action = :store_true
    end
    return parse_args(s)
end

function main()
    parsed_args = parse_commandline()

    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end

    ss = parsed_args["shape"]

    J1 = parsed_args["J1"]
    J2 = parsed_args["J2"]

    Jxs = parsed_args["Jx"]
    Jys = parsed_args["Jy"]
    Jzs = parsed_args["Jz"]

    shape = [ss[1] ss[3]; ss[2] ss[4]]

    max_dense = parsed_args["max-dense"]
    nev = parsed_args["nev"]

    random_seed = parsed_args["seed"]
    measure = parsed_args["measure"]
    force = parsed_args["force"]

    for (Jx, Jy, Jz) in Iterators.product(Jxs, Jys, Jzs)
        compute_disordered_kagome_trs(
            Jx, Jy, Jz,
            J1[1], J1[2],
            J2[1], J2[2],
            shape, max_dense, nev, random_seed, measure, force
        )
    end
end

main()
