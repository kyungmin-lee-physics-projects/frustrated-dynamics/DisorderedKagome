using LinearAlgebra
using Statistics
using Plots
import PyPlot
using Glob
using CodecXz
using MsgPack
using Formatting
using ProgressMeter
using ArgParse
PyPlot.matplotlib.use("Agg")


"""
    get_spacings(values, [digits=8])

Compute spacings between consecutive values, after rounding by digits and then sorting.
"""
function get_spacings(values::AbstractVector{<:Real}, digits::Integer=8)
    values2 = round.(sort(values); digits=digits)
    spacings = values2[2:end] - values2[1:end-1]
    return spacings
end


"""
    get_ratios(values, [digits=8])

Compute the ratios between spacings. Uses [`get_spacing`](@ref).
If the spacing is zero, use the next non-zero value.
"""
function get_ratios(values::AbstractVector{T}, digits::Integer=8) where {T<:AbstractFloat}
    spacings = get_spacings(values, digits)
    n = length(spacings)
    n < 2 && return T[]

    m = n
    while m > 1 && spacings[m] == 0
        m -= 1
    end

    m == 1 && return T[]

    s_prev = spacings[m] # nonzero
    ratios = T[]
    while m > 1
        m -= 1
        s = spacings[m]
        r = min(s, s_prev) / max(s, s_prev)
        if s != 0
            s_prev = s
        end
        push!(ratios, r)
        #print('mm=', m, 'len =', len(ratios))
    end
    return ratios
end


"""
    Form of GOE ratio function.
"""
function pdf_general(beta::Real)
    function pdf(x::Real)
        return (27.0/4) * (x+x^2)^(beta) / (1+x+x^2)^(1+3*beta/2)
    end
    # function pdf(x::AbstractArray{<:Real})
    #     return (27.0/4) .* (x + x.^2).^(beta) ./ (1 + x + x .^ 2)^(1 + 3 .* beta./2)
    # end
    return pdf
end

pdf_goe = pdf_general(1)
pdf_poisson(x::Real) = 2 / (1+x)^2
#pdf_poisson(x::AbstractArray{<:Real}) = 2 ./ (1+x).^2



"""
    read_directory(lattice_type, shape_str)

Read all *.msgpack.xz in directory `[datadir]/[lattice_type]/[shape_str]`.

# Return
* dictionary
  `(Jx, Jy, Jz, J1, J2, Sz) -> [[eigenvalues for one seed], [eigenvalues for another seed], ...]`.
  `J1` is `[J1min, J1max]`, and similarly for `J2`.
"""
function read_directory(lattice_type::AbstractString, shape_str::AbstractString)
    filepaths = glob("*.msgpack.xz", joinpath(@__DIR__, "..", "data", lattice_type, shape_str));

    database = Dict()

    println("Reading $(length(filepaths)) files")
    @showprogress for filepath in filepaths
        open(filepath, "r") do io
            ioc = XzDecompressorStream(io)
            data = MsgPack.unpack(ioc)
            close(ioc)
            parameter = data["parameter"]
            sector = data["sector"]
            J1 = Float64[parameter["J1"]...]
            J2 = Float64[parameter["J2"]...]
            Jx = Float64(parameter["Jx"])
            Jy = Float64(parameter["Jy"])
            Jz = Float64(parameter["Jz"])
            #shape = Int[parameter["shape"]...]
            seed = Int(parameter["seed"])

            Sz = Float64(sector["Sz"])
            dimension = Int(sector["dimension"])
            matrix_type = String(sector["matrix_type"])

            p = (Jx, Jy, Jz, J1, J2, Sz)
            if !haskey(database, p)
                database[p] = Vector{Float64}[]  # just eigenvalues
            end
            push!(database[p], data["eigenvalue"])
        end
    end
    return database
end



"""
    plot_cumulative(lattice_type, shape_str, database)

Plot cumulative level statistics in `database`, and save it to
`[datadir]/[lattice_type]/[shape_str]`.
"""
function plot_cumulative(
    lattice_type::AbstractString,
    shape_str::AbstractString,
    database::AbstractDict;
    extensions::AbstractVector{<:AbstractString},
)
    xs = 0:0.01:1
    ys_goe = pdf_goe.(xs)
    ys_poi = pdf_poisson.(xs)


    fig = PyPlot.figure(figsize=(8, 8))
    portion_sizes = [
        0.01, 0.02, 0.05,
        0.10, 0.20, 0.40,
        0.50, 0.80, 1.00,
    ]

    panel_shape = [3, 3]

    println("Plotting $(length(database)) items")
    @showprogress for ((Jx, Jy, Jz, (J1min, J1max), (J2min, J2max), Sz), all_eigenvalues) in database
        # out_filename = format(
        #     "level-statistics-ratio-cumulative_J1=({:.3f},{:.3f})_J2=({:.3f},{:.3f})_Jx={:.3f}_Jy={:.3f}_Jz={:.3f}_Sz={:.1f}_shape={:s}.svg",
        #     J1min, J1max, J2min, J2max, Jx, Jy, Jz, Sz, shape_str,
        # )

        ratios = [Vector{Float64}[] for p in portion_sizes]

        dimension = length(first(all_eigenvalues))
        count = length(all_eigenvalues)
        for eigenvalues in all_eigenvalues
            eigenvalue_min = minimum(eigenvalues)
            eigenvalue_max = maximum(eigenvalues)
            idx_select = [(eigenvalues .- eigenvalue_min) .<= (eigenvalue_max - eigenvalue_min) * r for r in portion_sizes]
            for (i, idx) in enumerate(idx_select)
                push!(ratios[i], collect(get_ratios(Float64[eigenvalues[idx]...])))
            end
        end

        fig.clf()
        axes = fig.subplots(panel_shape..., sharex=true,sharey=true)
        axes_flat = vcat(permutedims(axes, [2, 1])...)

        for (i,p) in enumerate(portion_sizes)
            title = "$(round(Int, p*100))%"
            r_portion = vcat(ratios[i]...)
            axes_flat[i].hist(r_portion, bins=100, density=true, alpha=0.75)
            axes_flat[i].text(
                0.95, 2.0,
                format("\$\\langle r \\rangle = {:.3f} \\pm {:.4f}\$", mean(r_portion), std(r_portion) / sqrt(length(r_portion))),
                ha="right", va="top", fontsize="x-small",
            )
            axes_flat[i].set_title(title, fontsize=8)
            axes_flat[i].plot(xs, ys_poi, linewidth=2, label="Poisson")
            axes_flat[i].plot(xs, ys_goe, linewidth=2, label="GOE")
        end
        axes_flat[1].set_xlim(0, 1)
        axes_flat[1].set_ylim(0, 2.2)

        axes[end,1].set_xlabel(raw"$\tilde{r}$", fontsize=12)
        axes[end,1].set_ylabel(raw"$P(\tilde{r})$", fontsize=12)
        #axes[end,1].legend(loc=1)
        fig.suptitle("$(lattice_type) $shape_str. \$ J_{z} = $(Jz), \\quad J_{1} \\in [$(J1min), $(J1max)), \\quad J_{2} \\in [$(J2min), $(J2max)) \$, dim = $(dimension), count = $(count)", fontsize=6)


        for ext in extensions
            out_filename = let
                prefix = "level-statistics-ratio-cumulative"
                parameter = format(
                    "J1=({:.3f},{:.3f})_J2=({:.3f},{:.3f})_Jx={:.3f}_Jy={:.3f}_Jz={:.3f}_Sz={:.1f}_shape={:s}",
                    J1min, J1max, J2min, J2max, Jx, Jy, Jz, Sz, shape_str
                )
                "$(prefix)_$(parameter).$(ext)"
            end
            out_filepath = joinpath(@__DIR__, "..", "plots", lattice_type, shape_str, out_filename)
            out_filedir = dirname(out_filepath)
            !isdir(out_filedir) && mkpath(out_filedir)
            fig.savefig(out_filepath, dpi=300, bbox_inches="tight")
        end
    end
end


"""
    plot_window(lattice_type::AbstractString, shape_str::AbstractString, database::AbstractDict)

Plot windowed level statistics in `database`, and save it to
`[datadir]/[lattice_type]/[shape_str]`.
"""
function plot_window(
    lattice_type::AbstractString,
    shape_str::AbstractString,
    database::AbstractDict;
    extensions::AbstractVector{<:AbstractString},
)
    xs = 0:0.01:1
    ys_goe = pdf_goe.(xs)
    ys_poi = pdf_poisson.(xs)

    fig = PyPlot.figure(figsize=(8, 10))
    portion_lowers = [
        0.00, 0.05, 0.10, 0.15,
        0.20, 0.25, 0.30, 0.35,
        0.40, 0.45, 0.50, 0.55,
        0.60, 0.65, 0.70, 0.75,
        0.80, 0.85, 0.90, 0.95,
    ]
    portion_uppers = [
        0.05, 0.10, 0.15, 0.20,
        0.25, 0.30, 0.35, 0.40,
        0.45, 0.50, 0.55, 0.60,
        0.65, 0.70, 0.75, 0.80,
        0.85, 0.90, 0.95, 1.00,
    ]

    panel_shape = [5, 4]

    println("Plotting $(length(database)) items")
    @showprogress for ((Jx, Jy, Jz, (J1min, J1max), (J2min, J2max), Sz), all_eigenvalues) in database

        ratios = [Vector{Float64}[] for p in portion_uppers]

        dimension = length(first(all_eigenvalues))
        count = length(all_eigenvalues)
        for eigenvalues in all_eigenvalues
            eigenvalue_min = minimum(eigenvalues)
            eigenvalue_max = maximum(eigenvalues)
            idx_select = [
                let lo = ((eigenvalue_max - eigenvalue_min) * portion_lowers[ir]) + eigenvalue_min,
                    hi = ((eigenvalue_max - eigenvalue_min) * portion_uppers[ir]) + eigenvalue_min
                    [lo <= ev <= hi for ev in eigenvalues]
                end
                    for ir in eachindex(portion_uppers)
            ]
            for (i, idx) in enumerate(idx_select)
                push!(ratios[i], collect(get_ratios(Float64[eigenvalues[idx]...])))
            end
        end

        fig.clf()
        axes = fig.subplots(panel_shape..., sharex=true, sharey=true)
        axes_flat = vcat(permutedims(axes, [2, 1])...)

        for (i, (plo, phi)) in enumerate(zip(portion_lowers, portion_uppers))
            title = "$(round(Int, plo*100))% - $(round(Int, phi*100))%"
            vals = vcat(ratios[i]...)
            if isempty(vals)
                continue
            end
            bins = min(50, floor(Int, length(vals)/16)+1)
            axes_flat[i].hist(vals, bins=bins, range=(0,1), density=true, alpha=0.75)
            axes_flat[i].text(
                0.95, 2.0,
                format("\$\\langle r \\rangle = {:.3f} \\pm {:.4f}\$", mean(vals), std(vals) / sqrt(length(vals))),
                ha="right", va="top", fontsize="x-small",
            )

            axes_flat[i].set_title(title, fontsize=8)
            axes_flat[i].plot(xs, ys_poi, linewidth=2, label="Poisson")
            axes_flat[i].plot(xs, ys_goe, linewidth=2, label="GOE")
        end
        axes_flat[1].set_xlim(0, 1)
        axes_flat[1].set_ylim(0, 2.2)

        axes[end,1].set_xlabel(raw"$\tilde{r}$", fontsize=12)
        axes[end,1].set_ylabel(raw"$P(\tilde{r})$", fontsize=12)
        #axes[end,1].legend(loc=1)
        fig.suptitle("$(lattice_type) $shape_str. \$ J_{z} = $(Jz), \\quad J_{1} \\in [$(J1min), $(J1max)), \\quad J_{2} \\in [$(J2min), $(J2max)) \$, dim = $(dimension), count = $(count)", fontsize=6)


        for ext in extensions
            out_filename = let
                prefix = "level-statistics-ratio-portion"
                parameter = format(
                    "J1=({:.3f},{:.3f})_J2=({:.3f},{:.3f})_Jx={:.3f}_Jy={:.3f}_Jz={:.3f}_Sz={:.1f}_shape={:s}",
                    J1min, J1max, J2min, J2max, Jx, Jy, Jz, Sz, shape_str
                )
                "$(prefix)_$(parameter).$(ext)"
            end
            out_filepath = joinpath(@__DIR__, "..", "plots", lattice_type, shape_str, out_filename)
            out_filedir = dirname(out_filepath)

            !isdir(out_filedir) && mkpath(out_filedir)

            fig.savefig(out_filepath, dpi=300, bbox_inches="tight")
        end
    end
end


function parse_commandline()
    s = ArgParseSettings(
        prog="",
        description="Plot the statistics of a disordered Kagome lattice",
    )
    @add_arg_table! s begin
        "lattice_type"
            arg_type = String
            required = true
        "shape"
            arg_type = String
            required = true
            nargs = '+'
        "--ext"
            arg_type = String
            nargs = '+'
            default = ["pdf"]
    end
    return parse_args(s)
end


function main()
    args = parse_commandline()
    for s in args["shape"]
        database = read_directory(args["lattice_type"], s)
        plot_cumulative(args["lattice_type"], s, database; extensions=args["ext"])
        plot_window(args["lattice_type"], s, database; extensions=args["ext"])
    end
end

main()
