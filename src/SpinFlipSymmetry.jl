struct BitFlipOperation<:AbstractSymmetryOperation{Int} end
struct BitFlipSymmetry <:AbstractSymmetry{SpinFlipOperation} end

LatticeTools.elements(sym::SpinFlipSymmetry) = [
    IdentityOperation(Int, 2),
    SpinFlipOperation(),
]
LatticeTools.element(sym::SpinFlipSymmetry, idx) = elements(sym)[idx]

function LatticeTools.irreps(sym::SpinFlipSymmetry)
    return [
        [ones(Int, (1,1)), ones(Int, (1,1))],
        [ones(Int, (1,1)), -ones(Int, (1,1))],
    ]
end
LatticeTools.irrep(sym::SpinFlipSymmetry, idx) = LatticeTools.irreps(sym)[idx]
LatticeTools.num_irreps(sym::SpinFlipSymmetry) = 2
LatticeTools.irrep_dimension(sym::SpinFlipSymmetry, idx) = 1

struct SpinFlipOperationEmbedding{BR<:Unsigned}<:LatticeTools.AbstractSymmetryOperationEmbedding
    nsites::Int
    mask::BR
    function SpinFlipOperationEmbedding{BR}(nsites::Integer) where BR
        mask = make_bitmask(nsites, BR)
        return new{BR}(nsites, mask)
    end
end

function QuantumHamiltonian.embed(
    hilbert_space::HilbertSpace{QN},
    op::SpinFlip
) where QN



struct SpinFlipSymmetryEmbedding{BR<:Unsigned} <: AbstractSymmetryEmbedding
    nsites::Int
    mask::BR
    function SpinFlipSymmetryEmbedding{BR}(nsites::Integer) where BR
        mask = make_bitmask(nsites, BR)
        return new{BR}(nsites, mask)
    end
end


LatticeTools.elements(sym::SpinFlipSymmetryEmbedding) = [
    IdentityOperation(Int, 2),
    SpinFlipOperation(),
]

function LatticeTools.irreps(sym::SpinFlipSymmetryEmbedding)
    return [
        [ones(Int, (1,1)), ones(Int, (1,1))],
        [ones(Int, (1,1)), -ones(Int, (1,1))],
    ]
end
LatticeTools.irrep(sym::SpinFlipSymmetryEmbedding, idx) = LatticeTools.irreps(sym)[idx]
LatticeTools.num_irreps(sym::SpinFlipSymmetryEmbedding) = 2
LatticeTools.irrep_dimension(sym::SpinFlipSymmetryEmbedding, idx) = 1


spin_flip_symmetry = SpinFlipSymmetry()
spin_flip_symmetry_embedding = SpinFlipSymmetryEmbedding{UInt}(n_sites)

spin_flip_operation_embedding = SpinFlipOperationEmbedding{UInt}(n_sites)

spin_flip_symmetry_embedding_irrep_components = [
    IrrepComponent(spin_flip_symmetry_embedding, 1, 1),
    IrrepComponent(spin_flip_symmetry_embedding, 2, 1),
]


function QuantumHamiltonian.symmetry_apply(
    hs::HilbertSpace{QN},
    spin_flip_operation_embedding::SpinFlipOperationEmbedding,
    bitrep::BR
) where {QN, BR<:Unsigned}
    return spin_flip_operation_embedding.mask & (~bitrep)
end


function QuantumHamiltonian.symmetry_apply(
    hs::HilbertSpace{QN},
    spin_flip_operation_embedding::SpinFlipOperationEmbedding,
    op::PureOperator{S, BR},
) where {QN, S<:Number, BR<:Unsigned}
    bm = op.bitmask
    br = symmetry_apply(hs, spin_flip_operation_embedding, op.bitrow) & bm
    bc = symmetry_apply(hs, spin_flip_operation_embedding, op.bitcol) & bm
    am = op.amplitude
    return PureOperator{S, BR}(bm, br ,bc, am)
end


QuantumHamiltonian.symmetry_apply(hilbert_space, spin_flip_operation_embedding, UInt(0x1))
