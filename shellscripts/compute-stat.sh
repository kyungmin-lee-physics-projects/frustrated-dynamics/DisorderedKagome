script=../scripts/compute-disordered-statistics.jl
parallel -j 3 julia $script triangular {} --window 0.01 ::: "(4,0)x(0,4)" "(2,-2)x(2,4)" "(6,0)x(0,6)" &
parallel -j 3 julia $script kagome {} --window 0.01 ::: "(2,-1)x(1,2)" "(2,0)x(0,2)" "(1,-2)x(2,3)" &
parallel -j 3 julia $script triangular-bond {} --window 0.01 ::: "(4,0)x(0,4)" &
parallel -j 3 julia $script kagome-obc {} --window 0.01 ::: "kagome-obc-5" &

parallel -j 3 julia $script triangular {} --window 0.05 ::: "(4,0)x(0,4)" "(2,-2)x(2,4)" "(6,0)x(0,6)" &
parallel -j 3 julia $script kagome {} --window 0.05 ::: "(2,-1)x(1,2)" "(2,0)x(0,2)" "(1,-2)x(2,3)" &
parallel -j 3 julia $script triangular-bond {} --window 0.05 ::: "(4,0)x(0,4)" &
parallel -j 3 julia $script kagome-obc {} --window 0.05 ::: "kagome-obc-5" &
wait
