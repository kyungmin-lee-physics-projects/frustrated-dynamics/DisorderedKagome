script=../scripts/plot-disordered-density-of-states.jl 
parallel -j 3 julia $script triangular {} --ext pdf svg png ::: "(4,0)x(0,4)" "(2,-2)x(2,4)" "(6,0)x(0,6)" &
parallel -j 3 julia $script kagome {} --ext pdf svg png ::: "(2,-1)x(1,2)" "(2,0)x(0,2)" "(1,-2)x(2,3)" &
parallel -j 3 julia $script triangular-bond {} --ext pdf svg png ::: "(4,0)x(0,4)" &
wait
